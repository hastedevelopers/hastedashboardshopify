
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button,
    Modal, 
    ModalHeader, 
    ModalBody, 
    ModalFooter,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText
} from 'reactstrap';
import { Card, Icon, Dropdown } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import DateTimePicker from 'react-datetime-picker'
import '../../../assets/css/theme.css';
import salesOverTimePic from '../../../assets/image/salesOverTime.jpg';

class PriceOptimization extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        
        this.state = {
            modal: false,
            date0: new Date(),
            date1: new Date(),
            selectedChannel: '1',
            channelOptions: [
                {
                    value:"1",
                    text:"All Channels"
                },
                {
                    value:"2",
                    text:"Development 1"
                },
                {
                    value:"3",
                    text:"Development 2"
                }
            ],
            selectedPricePoints: '1',
            pricePointsOptions: [
                {
                    value:"1",
                    text:"Original Price"
                },
                {
                    value:"2",
                    text:"$1.50"
                },
                {
                    value:"3",
                    text:"$2.50"
                }
            ],
            selectedProduct: '1',
            productOptions: [
                {
                    value:"1",
                    text:"Product 0"
                },
                {
                    value:"2",
                    text:"Product 1"
                },
                {
                    value:"3",
                    text:"Product 2"
                }
            ],
            selectedCategory: '1',
            categoryOptions: [
                {
                    value:"1",
                    text:"All Category"
                },
                {
                    value:"2",
                    text:"Category 1"
                },
                {
                    value:"3",
                    text:"Category 2"
                }
            ],
            selectedSubcategory: '1',
            subcategoryOptions: [
                {
                    value:"1",
                    text:"All Subcategory"
                },
                {
                    value:"2",
                    text:"Subcategory 1"
                },
                {
                    value:"3",
                    text:"Subcategory 2"
                }
            ],

        }
    }

    onChange0 = date0 => this.setState({ date0 });
    onChange1 = date1 => this.setState({ date1 });


    toggle() {
        this.setState({
          modal: !this.state.modal
        });
    }

    render(){
        return(
            <Card className="card-container" style={{height: '100%'}}>
                <Card.Content >
                    <Card.Header className="card-header">PRICE OPTIMIZATION</Card.Header>
                        <Line data={{
                            labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                            datasets: [
                                {
                                    label: 'Price Pt 1',
                                    data: [230, 290, 120, 300, 360, 260, 230, 280, 250],
                                    backgroundColor: 'rgba(244, 136, 130, 0.4)',
                                    borderColor: '#F48882',
                                    pointBorderColor: '#F48882',
                                    pointBackgroundColor: '#fff',
                                    pointHoverBackgroundColor: '#F48882',
                                    pointHoverBorderColor: '#F7A885',
                                },
                                {
                                    label: 'Price Pt 2',
                                    data: [330, 390, 220, 400, 460, 360, 330, 380, 350],
                                    backgroundColor: 'rgba(244, 136, 130, 0.4)',
                                    borderColor: '#F48882',
                                    pointBorderColor: '#F48882',
                                    pointBackgroundColor: '#fff',
                                    pointHoverBackgroundColor: '#F48882',
                                    pointHoverBorderColor: '#F7A885',
                                },
                                {
                                    label: 'Price Pt 3',
                                    data: [430, 490, 420, 600, 660, 460, 430, 480, 450],
                                    backgroundColor: 'rgba(244, 136, 130, 0.4)',
                                    borderColor: '#F48882',
                                    pointBorderColor: '#F48882',
                                    pointBackgroundColor: '#fff',
                                    pointHoverBackgroundColor: '#F48882',
                                    pointHoverBorderColor: '#F7A885',
                                }
                            ]
                        }} options={{scaleShowGridLines : true, }}  width={200} height={100} />

                        <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}} onClick={this.toggle}>
                            EDIT FILTER
                        </Button>
                    
                </Card.Content>
                <Modal isOpen={this.state.modal} toggle={this.toggle} size='lg' backdrop={false} className={this.props.className}>
                    <ModalHeader toggle={this.toggle} style={{fontSize: 17, paddingLeft: 25}}>PRICE OPTIMIZATION FILTER</ModalHeader>
                    <ModalBody style={{margin: 10}}>
                        <Row style={{marginTop: 15, marginBottom: 15}}>
                            <Col sm="7">
                                <Form>
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Product</Label>
                                        <Dropdown defaultValue={'1'} selection
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedProduct: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.productOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>CHANNEL</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedChannel: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.channelOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    {/* <Row>
                                        <Col>
                                            <Label style={{fontWeight: 'bold'}}>From</Label>
                                        </Col>
                                        <Col>
                                                <Label style={{fontWeight: 'bold'}}>To</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <FormGroup>
                                                <DateTimePicker onChange={this.onChange0} value={this.state.date0}/>
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup>
                                                <DateTimePicker onChange={this.onChange1} value={this.state.date1}/>
                                            </FormGroup>
                                        </Col>
                                    </Row> */}
                                    {/* <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Category</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedCategory: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.categoryOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup> */}
                                    {/* <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Sub Category</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedSubcategory: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.subcategoryOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup> */}
                                    
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Price Point(s)</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedPricePoints: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.pricePointsOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                </Form>
                                <Button style={{width: 180, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                    START OPTIMIZING
                                </Button>
                                {/* <Button style={{width: 180, marginLeft:20, backgroundColor: 'gray', color: 'white',  borderColor: 'white'}} onClick= {  async (event, data) => {
                                                    await this.setState({
                                                        selectedChannel: "",
                                        selectedPricePoints: "",
                                        selectedSubcategory: "",
                                        selectedProduct: "",
                                                    })
                                                }
                                 }>
                                    RESET TO DEFAULT
                                </Button> */}
                            </Col>
                            <Col sm="1"/>
                            <Col sm="4">
                                <img src={salesOverTimePic} alt="campaign" style={{width: '100%'}}/>
                                <p style={{marginTop: 10, marginBottom: 10, fontSize: 16, fontWeight: 'bold'}}>Optimize Price</p>
                                Select a product, followed by the channel(s).
                                <br/><br/>
                                Set price point(s) for the product to view forecasted sales and quantity sold over the next few months for different price point(s).
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal> 
            </Card>
        )
    }
}

export default PriceOptimization