
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button
} from 'reactstrap';
import { Card, Icon } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import '../../../assets/css/theme.css';
import campaignPic from '../../../assets/image/campaign.jpg';

class ProductFeatures extends Component {
    constructor(props) {
        super(props);
        this.productFeatureClick = this.productFeatureClick.bind(this);

        this.state = {
            productFeatures: this.props.productFeatures
        }

        console.log(this.props.productFeatures);
    }

    productFeatureClick(index){
        var data = this.state.productFeatures;
        data[index].active = !data[index].active;
        this.setState({
            productFeatures: data
        })
    }

    render(){
        const data= this.state.productFeatures;
        
        const rowStyle = {
            height: 100
        }

        return(
            <Card className="card-container">
                <Card.Content>
                    <Card.Header className="card-header">PRODUCT (FEATURES)</Card.Header>
                    <Row>
                        <Col style={{margin: 10}}>
                            <Row>
                                <Col onClick={() => {this.productFeatureClick(0)}}>
                                    <div className={data[0].active? "alchemistProductActive": "alchemistProduct"}>
                                        {
                                            data[0].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[0].name}</p>
                                    </div>
                                </Col>
                                <Col  onClick={() => {this.productFeatureClick(1)}}>
                                    <div className={data[1].active? "alchemistProductActive": "alchemistProduct"}>
                                        {
                                            data[1].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[1].name}</p>
                                    </div>
                                </Col>
                                <Col onClick={() => {this.productFeatureClick(2)}}>
                                    <div className={data[2].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[2].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[2].name}</p>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col onClick={() => {this.productFeatureClick(3)}}>
                                    <div className={data[3].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[3].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[3].name}</p>
                                    </div>
                                </Col>
                                <Col onClick={() => {this.productFeatureClick(4)}}>
                                    <div className={data[4].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[4].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[4].name}</p>
                                    </div>
                                </Col>
                                <Col onClick={() => {this.productFeatureClick(5)}}>
                                    <div className={data[5].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[5].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[5].name}</p>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col onClick={() => {this.productFeatureClick(6)}}>
                                    <div className={data[6].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[6].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[6].name}</p>
                                    </div>
                                </Col>
                                <Col onClick={() => {this.productFeatureClick(7)}}>
                                    <div className={data[7].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[7].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[7].name}</p>
                                    </div>
                                </Col>
                                <Col onClick={() => {this.productFeatureClick(8)}}>
                                    <div className={data[8].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[8].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[8].name}</p>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col onClick={() => {this.productFeatureClick(9)}}>
                                    <div className={data[9].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[9].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[9].name}</p>
                                    </div>
                                </Col>
                                <Col onClick={() => {this.productFeatureClick(10)}}>
                                    <div className={data[10].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[10].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[10].name}</p>
                                    </div>
                                </Col>
                                <Col onClick={() => {this.productFeatureClick(11)}}>
                                    <div className={data[11].active? "alchemistProductActive": "alchemistProduct"}>
                                    {
                                            data[11].active?
                                            <Icon name="check circle" className="summary-info-icon" style={{position: 'absolute', right: 0, top: 10}} size='large'/>
                                            :
                                            null
                                        }
                                        <p>{data[11].name}</p>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Card.Content>
            </Card>
        )
    }
}

export default ProductFeatures