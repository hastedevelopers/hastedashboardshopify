import React, { Component } from 'react';
import { 
    Container, 
    Row, 
    Col
} from 'reactstrap';
import { Card, Dropdown } from 'semantic-ui-react';
import '../../assets/css/theme.css';
import TopNavBar from '../Component/TopNavBar';
import Analysis from './Component/Analysis';
import Wiki from './Component/Wiki';
import alchemist1Pic from '../../assets/image/alchemist1.PNG';

export default class SaleForcasting extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            
        };
      }

    render() {

        return(
            <div className="grad">
                <TopNavBar name="alchemist"/>
            
                <Container>
                    <Row className="div-row">
                        <Col>
                            <h1>Alchemist</h1>
                        </Col>
                    </Row>
                    {/* <Row className="div-row">
                        <Card className="card-container">
                            <Card.Content>
                                <Row>
                                    <Col sm="8" style={{textAlign: 'left'}}>
                                        <p style={{marginTop: 10, fontSize: 16}}> Alchemist > Sales Forecasting</p>
                                    </Col>
                                </Row>
                            </Card.Content>
                        </Card>
                    </Row> */}
                    <Row className="div-row">
                        <Analysis/>
                    </Row>
                    <Row className="div-row">
                        <Wiki/>
                    </Row>
                </Container>
            
            </div>
        )
    }
}