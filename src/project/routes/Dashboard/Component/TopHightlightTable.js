
import React, {Component} from 'react';
import {
    Table
} from 'reactstrap';
import { Icon } from 'semantic-ui-react';
import '../../../assets/css/theme.css';

class TopHightlightTable extends Component {
    constructor(props) {
        super(props);
        this.quantityup = this.quantityup.bind(this);
        this.quantitydown = this.quantitydown.bind(this);
        this.totalup = this.totalup.bind(this);
        this.totaldown = this.totaldown.bind(this);
        this.quantitysort = this.quantitysort.bind(this);
        this.totalsort = this.totalsort.bind(this);
        this.state = {
            data: this.props.data,
            sort: null,
            lastsort: null
        }

        
    }

    componentDidMount() {
        this.totalsort();
    }

    quantityup(){
        let c = this.state.data;
        c.sort(function(a, b){return a.quantity-b.quantity});
        this.setState({
            data: c,
            lastsort:'quantity',
            sort: 'up'
        })
    }

    quantitydown(){
        let c = this.state.data;
        c.sort(function(a, b){return b.quantity-a.quantity});
        this.setState({
            data: c,
            lastsort:'quantity',
            sort: 'down'
        })
    }

    totalup(){
        let c = this.state.data;
        c.sort(function(a, b){return a.total-b.total});
        this.setState({
            data: c,
            lastsort:'total',
            sort: 'up'
        })
    }

    totaldown(){
        let c = this.state.data;
        c.sort(function(a, b){return b.total-a.total});
        this.setState({
            data: c,
            lastsort:'total',
            sort: 'down'
        })
    }
    
    quantitysort(){
        if(this.state.lastsort !== 'quantity'){
            this.quantitydown()
        }else{
            if(this.state.sort === 'up'){
                this.quantitydown()
            }else{
                this.quantityup()
            }
        }
    }

    totalsort(){
        if(this.state.lastsort !== 'total'){
            this.totaldown()
        }else{
            if(this.state.sort === 'up'){
                this.totaldown()
            }else{
                this.totalup()
            }
        }
    }

    render(){

        return(
            <Table className="table">
                <thead>
                    <tr>
                        <th>
                            {this.props.name}
                        </th>
                        <th>
                            <a onClick={this.quantitysort}>Quantity Sold 
                            { 
                                this.state.lastsort === 'quantity' && this.state.sort === 'up' ? <Icon name="angle up" color='grey' className="summary-info-icon"/>:null
                            }
                            
                            { 
                                this.state.lastsort === 'quantity' && this.state.sort === 'down' ? <Icon name="angle down" color='grey' className="summary-info-icon"/>:null
                            }
                            </a>
                        </th>
                        <th>
                            <a onClick={this.totalsort}>Total Sales 
                            { 
                                this.state.lastsort === 'total' && this.state.sort === 'up' ? <Icon name="angle up" color='grey' className="summary-info-icon"/>:null
                            }
                            
                            { 
                                this.state.lastsort === 'total' && this.state.sort === 'down' ? <Icon name="angle down" color='grey' className="summary-info-icon"/>:null
                            }
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.data.map(result =>
                        <tr key={result.name}>
                            <td>
                                <span className="popup" data-popuptext="Click to Investigate More">{result.name}</span> 
                            </td>
                            <td>{Number(result.quantity).toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</td>
                            <td>${Number(result.total).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}</td>
                        </tr>
                    )}
                </tbody>
            </Table>
        )
    }
}

export default TopHightlightTable