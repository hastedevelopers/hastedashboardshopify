import React, { Component } from 'react';
import { 
    Container, 
    Row, 
    Col
} from 'reactstrap';
import { Dropdown } from 'semantic-ui-react';

import '../../assets/css/theme.css';
import TopNavBar from '../Component/TopNavBar';
import SalesOverTime from './Component/SalesOverTime';
import Factors from './Component/Factors';
import ChannelComparison from './Component/ChannelComparison';
import PromotionalCampaign from './Component/PromotionalCampaign';

export default class Detective extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            
            selectedDuration: 1,
            durationOptions:[
                {
                    value:"1",
                    text:"Past 7 days"
                },
                {
                    value:"2",
                    text:"Past 30 days"
                },
                {
                    value:"3",
                    text:"Today"
                },
                {
                    value:"4",
                    text:"Last Week"
                },
                {
                    value:"5",
                    text:"Week to Date"
                },
                {
                    value:"6",
                    text:"Month to Date"
                },
                {
                    value:"7",
                    text:"Year to Date"
                }
            ],
            selectedStore: 1,
            storeOptions: [
                {
                    value:"1",
                    text:"All Channels"
                },
                {
                    value:"2",
                    text:"Development 1"
                },
                {
                    value:"3",
                    text:"Development 2"
                }
            ]
        };
      }



    render() {

        return(
            <div className="grad">
                <TopNavBar name="detective"/>
            
                <Container>
                    <Row className="div-row">
                        <Col>
                            <h1>Detective</h1>
                        </Col>
                    </Row>
                    <Row className="div-row">
                        <Col sm="9">
                            
                            <SalesOverTime/>
                            
                        </Col>
                        <Col sm="3">
                            <Factors/>
                        </Col>
                    </Row>
                    <Row className="div-row">
                        <Col>
                            
                            <ChannelComparison/>
                            
                        </Col>
                    </Row>
                    <Row className="div-row">
                        <Col>
                            
                            <PromotionalCampaign/>
                            
                        </Col>
                    </Row>
                </Container>
            
            </div>
        )
    }
}