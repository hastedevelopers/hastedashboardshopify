
import React, {Component} from 'react';
import { 
    Button,
    Table
} from 'reactstrap';
import { Card } from 'semantic-ui-react';
import '../../../assets/css/theme.css';

class LatestTransaction extends Component {
    constructor(props) {
        super(props);

        this.state = {
            transactions:this.props.transactionData
        }

    }
    componentWillReceiveProps(){
        const data  = this.props.transactionData;
        if(data!=undefined) {
            this.setState({transactions: data});
        }
    }
    render(){
        return(
            <Card className="card-container">
                <Card.Content style={{overflow: 'auto',maxHeight:500}}>
                    <Card.Header className="card-header">LATEST TRANSACTIONS</Card.Header>
                    <Table className="table">
                        <thead>
                            <tr>
                                <th>DateTime</th>
                                <th>Quantity</th>
                                <th>Profile</th>
                                <th>Outlet</th>
                                <th>Total Amount</th>
                            </tr>
                        </thead>
                        <tbody>

                        {this.state.transactions!=undefined? (
                            this.state.transactions.map((transactionData)=>{
                                return (
                                    <tr>
                                        <td>{transactionData.timestamp}</td>
                                        <td>{transactionData.number_of_items_purchased}</td>

                                        <td>{transactionData.age}   {transactionData.gender}</td>
                                        <td>{transactionData.store_location}</td>
                                        <td>{transactionData.total_receipt_amount}</td>
                                    </tr>
                                )

                            })
                        ):(null)}
                        </tbody>
                    </Table>
                    <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                        VIEW ALL TRANSACTIONS
                    </Button>
                </Card.Content>
            </Card>
        )
    }
}

export default LatestTransaction