import React, { Component } from 'react';
import './project/assets/css/App.css';
import { 
  Switch,
  Route,
  Redirect
 } from 'react-router-dom';

// Import Routes
import HomeScreen from './project/routes/Home/HomeScreen';
import Dashboard from './project/routes/Dashboard/Dashboard';
import Detective from './project/routes/Detective/Detective';
import Oracle from './project/routes/Oracle/Oracle';
import Alchemist from './project/routes/Alchemist/Alchemist';
import SaleForcasting from './project/routes/Alchemist/SaleForcasting';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          {/* Routes */}
          <Route path="/shopifyhaste" exact component={HomeScreen}/>
          <Route path="/dashboard" exact component = {Dashboard}/>
          <Route path="/detective" exact component = {Detective}/>
          <Route path="/oracle" exact component = {Oracle}/>
          <Route path="/alchemist" exact component = {Alchemist}/>
          <Route path="/salesforecasting" exact component = {SaleForcasting}/>
          <Route path="/sales"/>

          {/* Setting Default Paths */}
          <Redirect path="/" to="shopifyhaste"/>

        </Switch>
      </div>
    );
  }
}

export default App;
