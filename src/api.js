export const API_SERVER_URL = 'https://hastelive.com/portalbackend';


/*general*/
export const AVAILABLE_STORES= '/filterResource/getAllOutlets';

/*dashboard*/
export const TRANSACTIONS ='/dashboardResource/transactionsTable';
export const SUMMARY ='/dashboardResource/dashboardSummary';
export const TRANSACTION_TIME_GRAPH ='/dashboardResource/transactionsTimeGraph';
export const REVENUE_TIME_GRAPH ='/dashboardResource/revenueTimeGraph';
export const TOP_HIGHLIGHTS ='/dashboardResource/topHighlights';

