
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button,
    ListGroup, 
    ListGroupItem, 
    ListGroupItemHeading, 
    ListGroupItemText,
    Input,
    Popover,
    PopoverBody,
    Modal,  
    ModalHeader, 
    ModalBody, 
    ModalFooter,
} from 'reactstrap';
import { Card, Icon, Radio, Dropdown } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import '../../../assets/css/theme.css';
import alchemist1Pic from '../../../assets/image/alchemist1.PNG';
import alchemist4Pic from '../../../assets/image/alchemist4.PNG';
import Toggle from './Toggle';


class Analysis extends Component {
    constructor(props) {
        super(props);
        this.productCLick = this.productCLick.bind(this);
        this.toggle0 = this.toggle0.bind(this);
        this.toggle1 = this.toggle1.bind(this);
        this.toggle = this.toggle.bind(this);
        this.state = {
            holt: false,
            Weighted: true,
            popoverOpen0: false,
            popoverOpen1: false,
            modal: false,
            durationOptions: [
                { value: 'Weekly', text: 'Weeks' },
                { value: 'Monthly', text: 'Months' },
                { value: 'Yearly', text: 'Years' }
            ],
            selectedDuration: "Weekly",
        }
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
    }

    toggle0() {
        this.setState({
          popoverOpen0: !this.state.popoverOpen0
        });
    }

    toggle1() {
        this.setState({
          popoverOpen1: !this.state.popoverOpen1
        });
    }

    productCLick(index){
        if(index == 1){

            this.setState({
                holt: true,
                Weighted: false
            })

        }else{

            this.setState({
                holt: false,
                Weighted: true
            })

        }
    }

    render(){

        const liststyle= {border: 0, color: 'white', backgroundColor: '#383D53', cursor: 'pointer'};
        const liststyle2= {border: 0, color: 'white', backgroundColor: 'rgb(96, 100, 121)', cursor: 'pointer'};

        return(
            <Card className="card-container">
                <Card.Content style={{paddingBottom: 0}}>
                    <Card.Header className="card-header">
                        <img src={alchemist1Pic} alt="campaign" style={{width: 50}}/>
                        SALES FORECASTING
                    </Card.Header>
                    <Row>
                        <Col sm="3" style={{paddingLeft: 0, paddingBottom: 0, paddingRight: 0, backgroundColor: '#383D53'}}>
                            <ListGroup style={{width: '100%', textAlign: 'left', color: 'white'}}>
                                <ListGroupItem style={{border: 0, color: 'white', backgroundColor: 'rgb(79, 82, 90)'}}>
                                    <Icon name="search" size="large"  style={{position: 'absolute', left: 15, top: 14}}/>
                                    <Input style={{marginLeft: 30, marginTop: 3, backgroundColor: 'transparent', border: 'transparent', width: 230}} placeholder="Search methods ..." />
                                </ListGroupItem>
                                <ListGroupItem style={liststyle}>
                                    <Icon name="chart bar" size="large"  style={{position: 'absolute', left: 15, top: 14}}/>
                                    <p style={{marginLeft: 30, marginTop: 3}}>
                                        STATISTICAL MODELS
                                    </p>
                                </ListGroupItem>
                                <ListGroupItem style={this.state.holt?liststyle:liststyle2} onClick={()=> this.productCLick(2)}>
                                    <Icon size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                    <p style={{marginLeft: 30, marginTop: 3}}>
                                        Holt-Winters
                                    </p>
                                </ListGroupItem>
                                <ListGroupItem style={this.state.Weighted?liststyle:liststyle2} onClick={()=> this.productCLick(1)}>
                                    <Icon size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                    <p style={{marginLeft: 30, marginTop: 3}}>
                                        Weighted Moving Average
                                    </p>
                                </ListGroupItem>
                                <ListGroupItem style={liststyle}>
                                    <Icon name="braille" size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'white'}}/>
                                    <p style={{marginLeft: 30, marginTop: 3}}>
                                        NEURAL NETWORK MODELS
                                    </p>
                                </ListGroupItem>
                                <ListGroupItem style={liststyle}>
                                    <Icon size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                    <p style={{marginLeft: 30, marginTop: 3}}>
                                        NN Model A
                                    </p>
                                </ListGroupItem>
                                <ListGroupItem style={liststyle}>
                                    <Icon size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                    <p style={{marginLeft: 30, marginTop: 3}}>
                                        NN Model A
                                    </p>
                                </ListGroupItem>
                            </ListGroup>
                        </Col>
                        <Col sm="9" style={{textAlign: 'left', marginTop: 5}}>
                            {
                                this.state.holt?
                                <div >
                                    <Row>
                                        <Col sm="6">
                                            <span className="popup" data-popuptext="Statistical Model">
                                                <Icon size="large" name="chart bar" style={{position: 'absolute', left: 7, top: 7, color: 'gray'}}/>
                                            </span> 
                                            <p style={{marginTop: 2, marginLeft: 35, fontSize: 20}}>WEIGHTED MOVING AVERAGE</p>
                                        </Col> 
                                        <Col sm="6">
                                            <Row>
                                                <Col sm="3"/>
                                                <Col sm="2">
                                                    <span className="popup" data-popuptext="Run Method">
                                                        <Button style={{width: 50, backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}}>
                                                            <Icon name="play" size="large"/>
                                                        </Button>
                                                    </span>
                                                </Col>
                                                <Col sm="2">
                                                    <span className="popup" data-popuptext="Reset back to previously saved values">
                                                        <Button style={{width: 50, backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}}>
                                                            <Icon name="refresh" size="large"/>
                                                        </Button>
                                                    </span>
                                                </Col>
                                                <Col sm="2">
                                                    <span className="popup" data-popuptext="Save settings">
                                                        <Button style={{width: 50, backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}}>
                                                            <Icon name="save" size="large"/>
                                                        </Button>
                                                    </span>
                                                </Col>
                                                <Col sm="2">
                                                    <span className="popup" data-popuptext="Save us">
                                                        <Button style={{width: 50, backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}} onClick={this.toggle}>
                                                            <Icon name="cloud download" size="large"/>
                                                        </Button>
                                                    </span>
                                                </Col>
                                            </Row>
                                        </Col> 
                                    </Row>
                                    <Row style={{marginTop: 10, marginLeft: 3, fontSize: 18}}>
                                        <p>Set to Default?</p>  
                                        <span className="popup" data-popuptext="Set this as the default method">
                                            <Icon name="info circle" color='grey' id="Popover1" onClick={this.toggle0} style={{marginTop: 3, fontSize:20}}/>
                                        </span> 
                                        <Radio toggle style={{marginLeft: 10}}/>
                                    </Row>
                                    <Row style={{marginTop: 10, marginLeft: 3}}>
                                        <Col>
                                            <Row>
                                                <b>PARAMETERS</b>
                                            </Row>
                                            <Row>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 25}}>N</p>
                                                </Col>
                                                <Col sm ="2">
                                                    <Input value="4"/>
                                                </Col>
                                                <Col sm ="6">
                                                    <Dropdown defaultValue={'Weekly'} selection 
                                                        onChange={
                                                            async (event, data) => {
                                                                await this.setState({
                                                                    selectedDuration: data.value
                                                                })
                                                            }
                                                        }
                                                        options={
                                                            this.state.durationOptions
                                                        }
                                                    />
                                                </Col>
                                                <Col sm ="2">
                                                        <Toggle/>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 16}}>Weight</p>
                                                </Col>
                                                <Col sm ="8">
                                                    <Button style={{width: '100%', backgroundColor: 'white', color: '#f88186',  borderColor: '#f88186', borderRadius: 50}}>
                                                        Set Equal Weight
                                                    </Button>
                                                </Col>
                                                <Col sm ="2">
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                
                                                </Col>
                                                <Col sm ="2">
                                                    <Input value="4"/>
                                                </Col>
                                                <Col sm ="6" style={{textAlign: 'center'}}>
                                                    <p style={{marginTop: 6}}>[1 week ago]</p>
                                                </Col>
                                                <Col sm ="2">
                                                    
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                
                                                </Col>
                                                <Col sm ="2">
                                                    <Input value="4"/>
                                                </Col>
                                                <Col sm ="6" style={{textAlign: 'center'}}>
                                                    <p style={{marginTop: 6}}>[2 week ago]</p>
                                                </Col>
                                                <Col sm ="2">
                                                    
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                
                                                </Col>
                                                <Col sm ="2">
                                                    <Input value="4"/>
                                                </Col>
                                                <Col sm ="6" style={{textAlign: 'center'}}>
                                                    <p style={{marginTop: 6}}>[3 week ago]</p>
                                                </Col>
                                                <Col sm ="2">
                                                    
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                
                                                </Col>
                                                <Col sm ="2">
                                                    <Input value="4"/>
                                                </Col>
                                                <Col sm ="6" style={{textAlign: 'center'}}>
                                                    <p style={{marginTop: 6}}>[4 week ago]</p>
                                                </Col>
                                                <Col sm ="2">
                                                    
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <b>RESULTS</b>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 16}}>MAD</p>
                                                </Col>
                                                <Col sm ="8">
                                                    <Input value="1.412341234123423"/>
                                                </Col>
                                                <Col sm ="1" style={{paddingRight: 10}}>
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 16}}>MSE</p>
                                                </Col>
                                                <Col sm ="8">
                                                    <Input value="1.412341234123423"/>
                                                </Col>
                                                <Col sm ="1" style={{paddingRight: 10}}>
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row style={{marginTop: 10, marginLeft: 3}}>
                                        <Col sm="8">
                                            <Row  style={{marginTop: 20}}>

                                            </Row>
                                            <Row style={{marginTop: 20}}>

                                            </Row>
                                        </Col>
                                        <Col sm="4">
                                            
                                        </Col>
                                    </Row>
                                </div>
                                :
                                null
                            }

                            {
                                this.state.Weighted?
                                <div >
                                    <Row>
                                        <Col sm="6">
                                            <span className="popup" data-popuptext="Statistical Model">
                                                <Icon size="large" name="chart bar" style={{position: 'absolute', left: 7, top: 7, color: 'gray'}}/>
                                            </span> 
                                            <p style={{marginTop: 2, marginLeft: 35, fontSize: 20}}>HOLT-WINTERS</p>
                                        </Col> 
                                        <Col sm="6">
                                            <Row>
                                                <Col sm="3"/>
                                                <Col sm="2">
                                                    <span className="popup" data-popuptext="Run Method">
                                                        <Button style={{width: 50, backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}}>
                                                            <Icon name="play" size="large"/>
                                                        </Button>
                                                    </span>
                                                </Col>
                                                <Col sm="2">
                                                    <span className="popup" data-popuptext="Reset back to previously saved values">
                                                        <Button style={{width: 50, backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}}>
                                                            <Icon name="refresh" size="large"/>
                                                        </Button>
                                                    </span>
                                                </Col>
                                                <Col sm="2">
                                                    <span className="popup" data-popuptext="Save settings">
                                                        <Button style={{width: 50, backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}}>
                                                            <Icon name="save" size="large"/>
                                                        </Button>
                                                    </span>
                                                </Col>
                                                <Col sm="2">
                                                    <span className="popup" data-popuptext="Save us">
                                                        <Button style={{width: 50, backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}} onClick={this.toggle}>
                                                            <Icon name="cloud download" size="large"/>
                                                        </Button>
                                                    </span>
                                                </Col>
                                            </Row>
                                        </Col> 
                                    </Row>
                                    <Row style={{marginTop: 10, marginLeft: 3, fontSize: 18}}>
                                        <p>Set to Default?</p>  
                                        <span className="popup" data-popuptext="Set this as the default method">
                                            <Icon name="info circle" color='grey' id="Popover1" onClick={this.toggle0} style={{marginTop: 3, fontSize:20}}/>
                                        </span> 
                                        <Radio toggle style={{marginLeft: 10}}/>
                                    </Row>
                                    <Row style={{marginTop: 10, marginLeft: 3}}>
                                        <Col>
                                            <Row>
                                                <b>PARAMETERS</b>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="4" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 25}}>&part;</p>
                                                </Col>
                                                <Col sm ="6">
                                                    <Input value="0.4"/>
                                                </Col>
                                                <Col sm ="2">
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="4" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 25}}>&beta;</p>
                                                </Col>
                                                <Col sm ="6">
                                                    <Input value="0.6"/>
                                                </Col>
                                                <Col sm ="2">
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="4" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 25}}>&#8910;</p>
                                                </Col>
                                                <Col sm ="6">
                                                    <Input value="0.3"/>
                                                </Col>
                                                <Col sm ="2">
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="4" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 25}}>N</p>
                                                </Col>
                                                <Col sm ="6">
                                                    <Input value="8"/>
                                                </Col>
                                                <Col sm ="2">
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="4" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 18}}>N-Prediction</p>
                                                </Col>
                                                <Col sm ="6">
                                                    <Input value="8"/>
                                                </Col>
                                                <Col sm ="2">
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <b>RESULTS</b>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 16}}>MAD</p>
                                                </Col>
                                                <Col sm ="8">
                                                    <Input value="1.412341234123423"/>
                                                </Col>
                                                <Col sm ="1" style={{paddingRight: 10}}>
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                            <Row style={{marginTop: 10}}>
                                                <Col sm ="2" style={{paddingLeft: 0}}>
                                                <p style={{fontSize: 16}}>MSE</p>
                                                </Col>
                                                <Col sm ="8">
                                                    <Input value="1.412341234123423"/>
                                                </Col>
                                                <Col sm ="1" style={{paddingRight: 10}}>
                                                    <Toggle/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row style={{marginTop: 10, marginLeft: 3}}>
                                        
                                        <Col sm="4">
                                            
                                        </Col>
                                    </Row>
                                </div>
                                :
                                null
                            }
                            
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="3"  style={{paddingLeft: 0, paddingBottom: 0, paddingRight: 0}}>
                            <ListGroup style={{width: '100%', textAlign: 'left', color: 'white'}}>
                                <ListGroupItem style={{border: 0, color: 'white', backgroundColor: '#383D53'}}>
                                    
                                </ListGroupItem>
                                <ListGroupItem style={{border: 0, color: 'white', backgroundColor: '#383D53'}}>
                                    
                                </ListGroupItem>
                                <ListGroupItem style={{border: 0, color: 'white', backgroundColor: '#F48882', cursor: 'pointer'}}>
                                    <Icon name="plus circle" size="large"  style={{position: 'absolute', left: 15, top: 14}}/>
                                    <p style={{marginLeft: 30, marginTop: 3}}>
                                        ADD MODEL
                                    </p>
                                </ListGroupItem>
                            </ListGroup>
                        </Col>
                        <Col sm="9" style={{textAlign: 'left', marginTop: 70}}>
                        </Col>
                    </Row>
                </Card.Content>
                <Modal isOpen={this.state.modal} toggle={this.toggle} size='sm' backdrop={true} style={{zIndex: 2001}}>
                    <ModalHeader toggle={this.toggle} style={{fontSize: 17, paddingLeft: 25}}>Save As</ModalHeader>
                    <ModalBody style={{margin: 10}}>
                        <Row>
                            <Col sm="4">
                                <p style={{fontSize: 16}}>Name</p>
                            </Col>
                            <Col sm="8">
                                <Input placeholder={this.state.holt?"Weighted Moving Average":"Holt Winters"}/>
                            </Col>
                        </Row>
                        <Row style={{marginTop: 20}}>
                            <Col sm="4">
                                <p style={{fontSize: 16}}>Wiki</p>
                            </Col>
                            <Col sm="8">
                                <Input type="textarea" placeholder="Enter description here"/>
                            </Col>
                        </Row>
                        <Row style={{marginTop: 20}}>
                            <Col>
                                <Button style={{width: '100%', backgroundColor: '#f88186', color: 'white',  borderColor: '#f88186'}} onClick={this.toggle}>
                                    Save
                                </Button>
                            </Col>
                            <Col>
                                <Button style={{width: '100%', backgroundColor: 'red', color: 'white',  borderColor: 'red'}} onClick={this.toggle}>
                                    Cancel
                                </Button>
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal> 
            </Card>
        )
    }
}

export default Analysis