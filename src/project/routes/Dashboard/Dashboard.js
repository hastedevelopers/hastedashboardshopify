import React, { Component } from 'react';
import { 
    Container, 
    Row, 
    Col
} from 'reactstrap';
import { Dropdown } from 'semantic-ui-react';

import '../../assets/css/theme.css';
import TopNavBar from '../Component/TopNavBar';
import LatestTransaction from './Component/LatestTransaction';
import TopHightlight from './Component/TopHightlight';
import Overview from './Component/Overview';
import Summary from './Component/Summary';
import {
    API_SERVER_URL, AVAILABLE_STORES, REVENUE_TIME_GRAPH, SUMMARY, TOP_HIGHLIGHTS,
    TRANSACTION_TIME_GRAPH, TRANSACTIONS
} from "../../../api";

export default class Dashboard extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            merchant_name: "topplus",
            selectedDuration: "yeartodate",
            durationOptions: [
                {
                    value: "past7days",
                    text: "Past 7 days"
                },
                {
                    value: "past30days",
                    text: "Past 30 days"
                },
                {
                    value: "today",
                    text: "Today"
                },
                {
                    value: "lastweek",
                    text: "Last Week"
                },
                {
                    value: "weektodate",
                    text: "Week to Date"
                },
                {
                    value: "monthtodate",
                    text: "Month to Date"
                },
                {
                    value: "yeartodate",
                    text: "Year to Date"
                }
            ],
            selectedStores: [],
            storeOptions: [
            ],

            /*APICALLS*/
            summaryData: undefined,
            transactionData: [],
            revenueGraph: {
                label:"",
                data: [],
                labels: []
            },
            transactionGraph: {
                label:"",
                data: [],
                labels: []
            },
            highlights: undefined,
            totalSale :undefined

        };

    }

    async componentDidMount(){
          const allStores = await this.getAllStores();

          const storeOptions = allStores.map((stores) => {
              return {
                  "value": stores.outlet_code,
                  "text": stores.outlet_name
              }
          })
          // Default selected stores = all stores
          const selectedStores = storeOptions.map((stores) => {
              return stores.value;
          })
          this.setState({selectedStores:['HQ']}),
          this.setState({storeOptions:storeOptions}),
          await this.loadDashboard()
      }
    async loadDashboard(){
        await this.getSummary(this.state.selectedDuration,this.state.selectedStores);
        await this.getHighlights(this.state.selectedDuration,this.state.selectedStores);
        await this.getRevenueGraph(this.state.selectedDuration,this.state.selectedStores);
        await this.getTransactionGraph(this.state.selectedDuration,this.state.selectedStores);
        await this.getTransactions(this.state.selectedDuration,this.state.selectedStores);
    }
    async getAllStores(){
        const body = {
            "merchant_name": this.state.merchant_name
        }
        let allStores = await this.callPostAPI(AVAILABLE_STORES, body);

        //Only move on if there's no error
        if(!allStores.error){
            this.setState({
                storeOptions: allStores
            })
            return allStores;
        }else{
            this.setState({
                storeOptions: [],
            })
            return [];
        }
        
    }
    async getSummary(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);

        let data = await this.callPostAPI(SUMMARY, body);
        if(!data.error) {
            this.setState({
                summaryData: data
            });
            console.log(this.state.summaryData);
        }
    }
    async getTransactionGraph(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);
        let data = await this.callPostAPI(TRANSACTION_TIME_GRAPH, body);
        console.log(data);

        if(!data.error && data!=null){

            let labels = data.labels.map((value) => {
                if(value.length > 7){
                    let a = new Date(value);
                    var monthNames = [
                        "Jan", "Feb", "Mar",
                        "Apr", "May", "Jun", "Jul",
                        "Aug", "Sep", "Oct",
                        "Nov", "Dec"
                      ];

                    var day = a.getDate();
                    var monthIndex = a.getMonth();
                    var year = a.getYear();
                    return '' + day + ' ' + monthNames[monthIndex] + ' ' + year
                    
                }else{
                    return value
                }
            });

            let datas = {
                label:labels,
                data: data.datasets[0].data,
                labels: data.datasets[0].label,
            }
            
            this.setState({
                transactionGraph: datas
            })

            // console.log(this.state.transactionGraph);

        }
    }
    async getRevenueGraph(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);
        let data = await this.callPostAPI(REVENUE_TIME_GRAPH, body);
        console.log(data);
        if(!data.error){

            let labels = data.graph.labels.map((value) => {
                if(value.length > 7){
                    let a = new Date(value);
                    var monthNames = [
                        "Jan", "Feb", "Mar",
                        "Apr", "May", "Jun", "Jul",
                        "Aug", "Sep", "Oct",
                        "Nov", "Dec"
                      ];

                    var day = a.getDate();
                    var monthIndex = a.getMonth();
                    var year = a.getYear();
                    return '' + day + ' ' + monthNames[monthIndex] + ' ' + year
                    
                }else{
                    return value
                }
            });

            let datas = {
                label:labels,
                data: data.graph.datasets[0].data,
                labels: data.graph.datasets[0].label,
            }

            this.setState({
                revenueGraph: datas
            })
            
            console.log(this.state.revenueGraph);

            data.sales.map((value)=>{
                this.setState({
                    totalSale: "$" + this.numberFormatter(value.total_sales, 2, '.', ',')
                })
            })
        }
    }

    numberFormatter(number, fractionDigits = 0, thousandSeperator = ',', fractionSeperator = '.'){
        if (number!==0 && !number || !Number.isFinite(number)) return number
        let frDigits = Number.isFinite(fractionDigits)? Math.min(Math.max(fractionDigits, 0), 7) : 0
        const num = number.toFixed(frDigits).toString()
    
        const parts = num.split('.')
        let digits = parts[0].split('').reverse()
        let sign = ''
        if (num < 0) {sign = digits.pop()}
        let final = []
        let pos = 0
    
        while (digits.length > 1) {
            final.push(digits.shift())
            pos++
            if (pos % 3 === 0) {final.push(thousandSeperator)}
        }
        final.push(digits.shift())
        return `${sign}${final.reverse().join('')}${frDigits > 0 ? fractionSeperator : ''}${frDigits > 0 && parts[1] ? parts[1] : ''}`
    }

    async getTransactions(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);
        let data = await this.callPostAPI(TRANSACTIONS, body);
        if(!data.error){
            
            let datas = data.map((value) => {
                return {
                    age:value.age,
                    gender:value.gender,
                    number_of_items_purchased:value.number_of_items_purchased,
                    receipt_number:value.receipt_number,
                    store_location:value.store_location,
                    timestamp:value.timestamp,
                    total_receipt_amount: "$" + this.numberFormatter(value.total_receipt_amount, 2, '.', ',')
                }
            });
            
            this.setState({
                transactionData: datas
            })
            
            await console.log(this.state.transactionData);
        }
    }
    async getHighlights(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);

        let data = await this.callPostAPI(TOP_HIGHLIGHTS, body);
        if(!data.error){
            this.setState({
                highlights: data
            })
            await console.log(data)
        }
    }

    prepareFilterBody(selectedDuration, selectedStores){
        const body = {
            filter: selectedDuration,
            outlets: selectedStores,
            merchant_name: this.state.merchant_name
        }
        console.log(body);
        return body;
    }
    async callGetAPI(api){
        let result = {};
        await fetch(API_SERVER_URL + api,{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
            })

        return result;
    }
    async callPostAPI(api, body){
        let result = {};

        await fetch(API_SERVER_URL + api,{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
            })
        return result;
    }

    async refreshData() {
        // Check if there are stores selected
        let selectedStores = this.state.selectedStores;
        let selectedDuration = this.state.selectedDuration;

        let storeOptions = this.state.storeOptions;

        if (selectedStores.length === 0) {
            selectedStores = storeOptions;
        } else {
            let result = storeOptions.filter((curStore, i) => {
                return selectedStores.indexOf(curStore.value) !== -1;
            });

            selectedStores = result;
            if (selectedDuration === null) {
                selectedDuration = "week"
            }
            await this.loadDashboard();
        }
    }



    render() {

        return(
            <div className="grad">
                <TopNavBar name="dashboard"/>
            
                <Container>
                    <Row className="div-row">
                        <Col sm="7">
                            <h1>Dashboard</h1>

                        </Col>
                        <Col sm="5">
                            <Row>
                            <Col>
                                <Dropdown defaultValue={['HQ']} selection multiple
                                    onChange={
                                        async (event, data) => {
                                            console.log(data.value)
                                            await this.setState({
                                                selectedStore: data.value
                                            }), this.refreshData()
                                        }
                                    }
                                    options={
                                        this.state.storeOptions
                                    } 
                                />
                            </Col>
                            <Col>
                                <Dropdown defaultValue={'past7days'} selection 
                                    onChange={
                                        async (event, data) => {
                                            await this.setState({
                                                selectedDuration: data.value
                                            }), this.refreshData()
                                        }
                                    }
                                    options={
                                        this.state.durationOptions
                                    }
                                />
                            </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Summary summaryData={this.state.summaryData}/>
                    <Row className="div-row">
                        <Col sm="7">
                            <Row>
                                <LatestTransaction
                                    transactionData = {this.state.transactionData}/>
                            </Row>
                            <Row>
                                <TopHightlight data = {this.state.highlights}/>
                            </Row>
                        </Col>
                        <Col sm="5">
                            <Overview
                                salesData={this.state.totalSale}
                                revenueGraph={this.state.revenueGraph}
                                transactionsGraph={this.state.transactionGraph}
                            />
                        </Col>
                    </Row>
                </Container>
            
            </div>
        )
    }
}