
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button
} from 'reactstrap';
import { Card, Icon } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import '../../../assets/css/theme.css';
import alchemist4Pic from '../../../assets/image/alchemist4.PNG';

class Wiki extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
        }
    }

    render(){
        return(
            <Card className="card-container">
                <Card.Content>
                    <Card.Header className="card-header">WIKI</Card.Header>
                    <Row>
                        <Col sm="4">
                            <img src={alchemist4Pic} alt="campaign"/>
                        </Col>
                        <Col sm="8" style={{textAlign: 'left', marginTop: 70}}>
                            <p style={{marginTop: 10, fontSize: 30}}> Hang In There ...</p>
                            <p style={{marginTop: 20, fontSize: 24, color: 'gray'}}> We're sorry, this feature is still under construction</p>
                            <p style={{marginTop: 10, fontSize: 24, color: 'gray'}}> Please check back later. </p>
                        </Col>
                    </Row>
                </Card.Content>
            </Card>
        )
    }
}

export default Wiki