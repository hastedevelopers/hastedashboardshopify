
import React, {Component} from 'react';
import { 
    Button,
    Row, 
    Col,
    ButtonGroup,
    Modal,  
    ModalHeader, 
    ModalBody, 
    ModalFooter,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText
} from 'reactstrap';
import { Card, Icon, Dropdown } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import DateTimePicker from 'react-datetime-picker'
import '../../../assets/css/theme.css';
import salesOverTimePic from '../../../assets/image/salesOverTime.jpg';

class SalesOverTime extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggle1 = this.toggle1.bind(this);
        this.toggle2 = this.toggle2.bind(this);
        this.toggle3 = this.toggle3.bind(this);
        
        this.state = {
            groupBtnActive: 'weekly',
            modal: false,
            date0: new Date(),
            date1: new Date(),
            selectedChannel: '1',
            selectedDuration: "Daily",
            channelOptions: [
                {
                    value:"1",
                    text:"All Channels"
                },
                {
                    value:"2",
                    text:"Development 1"
                },
                {
                    value:"3",
                    text:"Development 2"
                }
            ],
            selectedCategory: '1',
            categoryOptions: [
                {
                    value:"1",
                    text:"All Category"
                },
                {
                    value:"2",
                    text:"Category 1"
                },
                {
                    value:"3",
                    text:"Category 2"
                }
            ],
            selectedSubcategory: '1',
            subcategoryOptions: [
                {
                    value:"1",
                    text:"All Subcategory"
                },
                {
                    value:"2",
                    text:"Subcategory 1"
                },
                {
                    value:"3",
                    text:"Subcategory 2"
                }
            ],
            selectedProduct: '1',
            productOptions: [
                {
                    value:"1",
                    text:"All Product"
                },
                {
                    value:"2",
                    text:"Product 1"
                },
                {
                    value:"3",
                    text:"Product 2"
                }
            ],
            durationOptions: [
                { value: 'Daily', text: 'Daily' }, 
                { value: 'Weekly', text: 'Weekly' },
                { value: 'Monthly', text: 'Monthly' },
                { value: 'Quarterly', text: 'Quarterly'},
                { value: 'Yearly', text: 'Yearly' }
            ],
            selectedDuration: "Daily",
        }
        
    }

    onChange0 = date0 => this.setState({ date0 });
    onChange1 = date1 => this.setState({ date1 });


    toggle() {
        this.setState({
          modal: !this.state.modal
        });
    }

    toggle1(value){
        this.setState({
            groupBtnActive: 'weekly'
        })
    }

    toggle2(value){
        this.setState({
            groupBtnActive: 'monthly'
        })
    }

    toggle3(value){
        this.setState({
            groupBtnActive: 'yearly'
        })
    }

    render(){
        let stylebtngroup = {
            minWidth: 150, 
            borderRadius: 50,
            backgroundColor: 'white', 
            color: '#f88186',
            borderColor: '#f88186',
            borderWidth: 2
        }
        let stylebtngroupoutline = {
            minWidth: 150, 
            borderRadius: 50,
            backgroundColor: 'transparent', 
            color: '#9e9e9e4d',
            borderColor: 'transparent',
            borderWidth: 2
        }
        return(
            <Card className="card-container" style= {{height: '100%'}}>
                <Card.Content>
                    <Card.Header className="card-header">SALES OVER TIME</Card.Header>
                    <ButtonGroup style={{margin: 40}}>
                        <div style={{borderRadius: 50, borderColor: '#9e9e9e4d', padding: 0, backgroundColor: 
                    'white', boxShadow: '0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5'}}>
                            <Button style={this.state.groupBtnActive === 'weekly'?stylebtngroup:stylebtngroupoutline} onClick={this.toggle1}>WEEKLY</Button>
                            <Button style={this.state.groupBtnActive === 'monthly'?stylebtngroup:stylebtngroupoutline} onClick={this.toggle2}>MONTHLY</Button>
                            <Button style={this.state.groupBtnActive === 'yearly'?stylebtngroup:stylebtngroupoutline} onClick={this.toggle3}>YEARLY</Button>
                        </div>
                    </ButtonGroup>
                    <Row style={{marginLeft: 6, marginRight:6}}>
                        <Line data={{
                            labels: ["Jan 18", "Jan 19", "Jan 20", "Jan 21", "Jan 22", "Jan 23", "Jan 24","Jan 25","Jan 26"],
                            datasets: [
                                {
                                    label: 'Sales Over Time',
                                    data: [100, 210, 230, 180, 435, 610, 430, 602, 390],
                                    backgroundColor: 'rgba(244, 136, 130, 0.4)',
                                    borderColor: '#F48882',
                                    pointBorderColor: '#F48882',
                                    pointBackgroundColor: '#fff',
                                    pointHoverBackgroundColor: '#F48882',
                                    pointHoverBorderColor: '#F7A885',
                                }
                            ]
                        }} options={{scaleShowGridLines : true}} width={200} height={100} />
                    </Row>
                    <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}} onClick={this.toggle}>
                        EDIT FILTER
                    </Button>
                </Card.Content>
                <Modal isOpen={this.state.modal} toggle={this.toggle} size='lg' backdrop={false} className={this.props.className}>
                    <ModalHeader toggle={this.toggle} style={{fontSize: 17, paddingLeft: 25}}>SALES OVER TIME FILTER</ModalHeader>
                    <ModalBody style={{margin: 10}}>
                        <Row style={{marginTop: 15, marginBottom: 15}}>
                            <Col sm="7">
                                <Form>
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>CHANNEL</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedChannel: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.channelOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>FREQUENCY</Label>
                                        <Dropdown defaultValue={'Daily'} selection
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedDuration: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.durationOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    <Row>
                                        <Col>
                                            <Label style={{fontWeight: 'bold'}}>From</Label>
                                        </Col>
                                        <Col>
                                                <Label style={{fontWeight: 'bold'}}>To</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <FormGroup>
                                                <DateTimePicker onChange={this.onChange0} value={this.state.date0}/>
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup>
                                                <DateTimePicker onChange={this.onChange1} value={this.state.date1}/>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Category</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedCategory: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.categoryOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Sub Category</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedSubcategory: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.subcategoryOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Product</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedProduct: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.productOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                </Form>
                                <Button style={{width: 180, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                    FILTER RESULT
                                </Button>
                                <Button style={{width: 180, marginLeft:20, backgroundColor: 'gray', color: 'white',  borderColor: 'white'}} onClick= {  async (event, data) => {
                                                    await this.setState({
                                                        selectedChannel: "",
                                        selectedCategory: "",
                                        selectedSubcategory: "",
                                        selectedProduct: "",
                                                    })
                                                }
                                 }>
                                    RESET TO DEFAULT
                                </Button>
                            </Col>
                            <Col sm="1"/>
                            <Col sm="4">
                                <img src={salesOverTimePic} alt="campaign" style={{width: '100%'}}/>
                                <p style={{marginTop: 10, marginBottom: 10, fontSize: 16, fontWeight: 'bold'}}>Sales Over Time Graph</p>
                                    Filter by channel(s) and date range, then select category, subcategory, product. The result will be ONE combined sales graph for the selected category, subcategory and product across the selected date range and channel(s).
                                    <br/><br/>
                                    Reset to Default will reset the filters to "All Channels", "All Categories" for the past 7 days.
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal> 
            </Card>
        )
    }
}

export default SalesOverTime