
import React, {Component} from 'react';
import { 
    Row, 
    Col,
} from 'reactstrap';
import { Card, Icon } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import '../../../assets/css/theme.css';

class Overview extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            sales : undefined,
            transactionsLabel:undefined,
            transactionsData:undefined,
            transactionsHeader:undefined,
            revenueLabel:undefined,
            revenueData:undefined,
            revenueHeader:undefined
        }
    }
    componentWillReceiveProps(){
        const dataTransaction = this.props.transactionGraph;
        const dataRevenue = this.props.revenueGraph;
        const dataSale  = this.props.salesData;
        if(dataSale!=undefined) {
            this.setState({sales: dataSale});
        }
        if(dataTransaction!=undefined){

        }
        if(dataRevenue!=undefined){

        }
    }
    render(){
        return(
            <Card className="card-container">
                <Card.Content style={{textAlign: 'left'}}>
                    <Card.Header className="card-header">OVERVIEW</Card.Header>
                    <Row className="card-subheader">
                        Total Sales
                    </Row>
                    <Row className="overview-border-bottom">
                        <Col sm="8" className="card-subheader">
                            <h1>{this.props.salesData}</h1>
                            
                        </Col>
                        {/* <Col sm="3">
                            <Icon name="arrow right" color='grey' className="summary-info-icon" style={{position: 'absolute', right: 0, bottom: 10}}/>
                        </Col> */}
                    </Row>
                    <Row className="card-subheader">
                        Transactions
                    </Row>
                    <Row className="card-subheader">
                        
                        <Line data={{labels: this.props.transactionsGraph.label,
                            datasets: [
                                {
                                    label: this.props.transactionsGraph.labels,
                                    data: this.props.transactionsGraph.data,
                                    backgroundColor: 'rgba(244, 136, 130, 0.4)',
                                    borderColor: '#F48882',
                                    pointBorderColor: '#F48882',
                                    pointBackgroundColor: '#fff',
                                    pointHoverBackgroundColor: '#F48882',
                                    pointHoverBorderColor: '#F7A885',
                                }
                            ]}}
                         options={{scaleShowGridLines : true}} width={200} height={100}/>
                    </Row>
                    {/* <Row style={{color:'#f88186'}} className="overview-border-bottom">
                        <Col sm="8" className="card-subheader">
                            View complete report
                        </Col>
                        <Col sm="3">
                            <Icon name="arrow right" className="summary-info-icon" style={{position: 'absolute', right: 0, bottom: 5, color: '#f88186'}}/>
                        </Col>
                    </Row> */}
                    <Row className="card-subheader">
                        Revenue
                    </Row>
                    <Row className="card-subheader" style={{height:200}}>
                        <Line data={{
                            labels: this.props.revenueGraph.label,
                            datasets: [
                                {
                                    label: this.props.revenueGraph.labels,
                                    data: this.props.revenueGraph.data,
                                    backgroundColor: 'rgba(244, 136, 130, 0.4)',
                                    borderColor: '#F48882',
                                    pointBorderColor: '#F48882',
                                    pointBackgroundColor: '#fff',
                                    pointHoverBackgroundColor: '#F48882',
                                    pointHoverBorderColor: '#F7A885',
                                }
                            ]
                        }} options={{scaleShowGridLines : true}} width={200} height={100}/>
                    </Row>
                    {/* <Row style={{color:'#f88186', margin:0}}>
                        <Col sm="8" className="card-subheader">
                            View complete report 
                        </Col>
                        <Col sm="3">
                            <Icon name="arrow right" className="summary-info-icon" style={{position: 'absolute', right: 0, bottom: 5, color: '#f88186'}}/>
                        </Col>
                    </Row> */}
                </Card.Content>
            </Card>
        )
    }
}

export default Overview