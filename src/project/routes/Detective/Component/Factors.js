
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    ListGroup, 
    ListGroupItem, 
    ListGroupItemHeading, 
    ListGroupItemText
} from 'reactstrap';
import { Card, Icon } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import '../../../assets/css/theme.css';

class Factors extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
        }
    }

    render(){
        return(
            <Card className="card-container" style={{height: '100%'}}>
                <Card.Content style={{textAlign: 'left'}}>
                    <Card.Header className="card-header">TOP 5 FACTORS</Card.Header>
                    <Row>
                        <ListGroup style={{width: '100%'}}>
                            <ListGroupItem style={{border: 0, marginLeft: 3, marginRight: 3}}>
                                <Icon name="calendar alternate outline" size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                <ListGroupItemHeading style={{marginBottom: 0, marginTop: 0, marginLeft: 30}}>
                                    Hari Raya Muslim
                                </ListGroupItemHeading>
                                <ListGroupItemText style={{color: 'gray', marginLeft: 30}}>
                                    25 June 2018
                                </ListGroupItemText>
                            </ListGroupItem>
                            <ListGroupItem style={{border: 0, marginLeft: 3, marginRight: 3}}>
                                <Icon name="calendar alternate outline" size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                <ListGroupItemHeading style={{marginBottom: 0, marginTop: 0, marginLeft: 30}}>
                                    Hari Raya Muslim
                                </ListGroupItemHeading>
                                <ListGroupItemText style={{color: 'gray', marginLeft: 30}}>
                                    25 June 2018
                                </ListGroupItemText>
                            </ListGroupItem>
                            <ListGroupItem style={{border: 0, marginLeft: 3, marginRight: 3}}>
                                <Icon name="calendar alternate outline" size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                <ListGroupItemHeading style={{marginBottom: 0, marginTop: 0, marginLeft: 30}}>
                                    Hari Raya Muslim
                                </ListGroupItemHeading>
                                <ListGroupItemText style={{color: 'gray', marginLeft: 30}}>
                                    25 June 2018
                                </ListGroupItemText>
                            </ListGroupItem>
                            <ListGroupItem style={{border: 0, marginLeft: 3, marginRight: 3}}>
                                <Icon name="calendar alternate outline" size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                <ListGroupItemHeading style={{marginBottom: 0, marginTop: 0, marginLeft: 30}}>
                                    Hari Raya Muslim
                                </ListGroupItemHeading>
                                <ListGroupItemText style={{color: 'gray', marginLeft: 30}}>
                                    25 June 2018
                                </ListGroupItemText>
                            </ListGroupItem>
                            <ListGroupItem style={{border: 0, marginLeft: 3, marginRight: 3}}>
                                <Icon name="calendar alternate outline" size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                <ListGroupItemHeading style={{marginBottom: 0, marginTop: 0, marginLeft: 30}}>
                                    Hari Raya Muslim
                                </ListGroupItemHeading>
                                <ListGroupItemText style={{color: 'gray', marginLeft: 30}}>
                                    25 June 2018
                                </ListGroupItemText>
                            </ListGroupItem>
                            <ListGroupItem style={{border: 0, marginLeft: 3, marginRight: 3}}>
                                <Icon name="calendar alternate outline" size="large"  style={{position: 'absolute', left: 15, top: 14, color: 'gray'}}/>
                                <ListGroupItemHeading style={{marginBottom: 0, marginTop: 0, marginLeft: 30}}>
                                    Hari Raya Muslim
                                </ListGroupItemHeading>
                                <ListGroupItemText style={{color: 'gray', marginLeft: 30}}>
                                    25 June 2018
                                </ListGroupItemText>
                            </ListGroupItem>
                        </ListGroup>
                    </Row>
                </Card.Content>
            </Card>
        )
    }
}

export default Factors