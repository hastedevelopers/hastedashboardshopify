
import React, {Component} from 'react';
import { 
    Button,
    Collapse,
    Navbar,
    NavbarToggler,
    // NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';

import { Icon } from 'semantic-ui-react';
import '../../assets/css/theme.css';

class TopNavBar extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
        }
    }

    toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
    }
    
    render(){
        let navItem={
            paddingRight: 30
        }
        let rightNavItem={
            marginRight: 10
        }
        return(
            <Navbar className="nav-border-bottom" color="light" light expand="md">
                {/* <NavbarBrand href="/">Haste</NavbarBrand> */}
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem style={navItem} className="nav-border-left" active={ this.props.name === "dashboard"? true:false }>
                            <NavLink href="/dashboard">DASHBOARD</NavLink>
                        </NavItem>
                        <NavItem style={navItem} active={ this.props.name === "detective"? true:false }>
                            <NavLink href="/detective">DETECTIVE</NavLink>
                        </NavItem>
                        <NavItem style={navItem} active={ this.props.name === "oracle"? true:false }>
                            <NavLink href="/oracle">ORACLE</NavLink>
                        </NavItem>
                        <NavItem style={navItem} active={ this.props.name === "alchemist"? true:false }>
                            <NavLink href="/alchemist">ALCHEMIST</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <UncontrolledDropdown nav inNavbar style={rightNavItem}>
                            <DropdownToggle nav caret>
                            Admin X
                            </DropdownToggle>
                            <DropdownMenu right>
                            <DropdownItem>
                                View Account
                            </DropdownItem>
                            <DropdownItem>
                                Change Password
                            </DropdownItem>
                            <DropdownItem divider />
                            <DropdownItem>
                                Logout
                            </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        <Button color="link" style={rightNavItem}>
                            <Icon size="large" name="bell" color='grey' />
                        </Button>
                        <Button color="link" style={rightNavItem}>
                            <Icon size="large" name="search" color='grey' />
                        </Button>
                    </Nav>
                </Collapse>
            </Navbar>
        )
    }
}

export default TopNavBar