import React, { Component } from 'react';
import { 
    Container, 
    Row, 
    Col
} from 'reactstrap';
import { Dropdown } from 'semantic-ui-react';

import '../../assets/css/theme.css';
import TopNavBar from '../Component/TopNavBar';
import ForecastedSales from './Component/ForecastedSales';
import PriceOptimization from './Component/PriceOptimization';
import PromotionRecommendation from './Component/PromotionRecommendation';

export default class Oracle extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            
        };
      }



    render() {

        return(
            <div className="grad">
                <TopNavBar name="oracle"/>
            
                <Container>
                    <Row className="div-row">
                        <Col>
                            <h1>Oracle</h1>
                        </Col>
                    </Row>
                    <Row className="div-row">
                        <ForecastedSales/>
                    </Row>
                    <Row className="div-row">
                        <Col sm="6">
                            
                            <PriceOptimization/>
                            
                        </Col>
                        <Col sm="6">
                            
                            <PromotionRecommendation/>
                            
                        </Col>
                    </Row>
                </Container>
            
            </div>
        )
    }
}