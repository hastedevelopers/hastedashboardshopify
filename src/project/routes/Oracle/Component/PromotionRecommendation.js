
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button,
    Modal, 
    ModalHeader, 
    ModalBody, 
    ModalFooter,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText
} from 'reactstrap';
import { Card, Icon, Dropdown } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import DateTimePicker from 'react-datetime-picker'
import '../../../assets/css/theme.css';
import salesOverTimePic from '../../../assets/image/salesOverTime.jpg';

class PromotionRecommendation extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {

            groupBtnActive: 'weekly',
            modal: false,
            date0: new Date(),
            date1: new Date(),
            selectedChannel: '1',
            channelOptions: [
                {
                    value:"1",
                    text:"All Channels"
                },
                {
                    value:"2",
                    text:"Development 1"
                },
                {
                    value:"3",
                    text:"Development 2"
                }
            ],
            selectedPromotionType: '1',
            promotionTypeOptions: [
                {
                    value:"1",
                    text:"All Promotion Type"
                },
                {
                    value:"2",
                    text:"Promotion Type 1"
                },
                {
                    value:"3",
                    text:"Promotion Type 2"
                }
            ],
            selectedProduct: '1',
            productOptions: [
                {
                    value:"1",
                    text:"All Product"
                },
                {
                    value:"2",
                    text:"Product 1"
                },
                {
                    value:"3",
                    text:"Product 2"
                }
            ]
        }
    }

    onChange0 = date0 => this.setState({ date0 });
    onChange1 = date1 => this.setState({ date1 });

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
    }

    render(){
        let Rowboxstyle = {textAlign: 'left', marginLeft: 10};
        let ColboxstyleLeft = {height: 60, backgroundColor: '#f8f9fa', marginBottom:10, marginRight: 10};
        let ColboxstyleRight = {height: 60, backgroundColor: '#f8f9fa', marginBottom:10, marginRight: 30};
        let ColBoxFontStyle = {color: 'black', fontWeight: 'bold', fontSize: 16, marginTop: 6}

        return(
            <Card className="card-container" style={{height: '100%'}}>
                <Card.Content >
                    <Card.Header className="card-header">PROMOTION RECOMMENDATION</Card.Header>
                        <Line data={{
                            labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                            datasets: [
                                {
                                    label: 'With Promotion',
                                    data: [230, 290, 420, 300, 360, 260, 230, 150, 250],
                                    backgroundColor: 'transparent',
                                    borderColor: 'green',
                                    pointBorderColor: 'green',
                                    pointBackgroundColor: '#fff',
                                    pointHoverBackgroundColor: 'green',
                                    pointHoverBorderColor: '#F7A885',
                                },
                                {
                                    label: 'Without Promotion',
                                    data: [330, 390, 120, 400, 460, 360, 330, 200, 450],
                                    backgroundColor: 'transparent',
                                    borderColor: '#F48882',
                                    pointBorderColor: '#F48882',
                                    pointBackgroundColor: '#fff',
                                    pointHoverBackgroundColor: '#F48882',
                                    pointHoverBorderColor: '#F7A885',
                                }
                            ]
                        }} options={{scaleShowGridLines : true}} width={200} height={100} />

                        <Row style={Rowboxstyle}>
                            <Col style={ColboxstyleLeft}>
                                <p style={ColBoxFontStyle}>Original Selling Price</p>
                                <p style={{color: '#F48882'}}>$0</p>
                            </Col>
                            <Col style={ColboxstyleRight}>
                                <p style={ColBoxFontStyle}>% Profit Margin (Original)</p>
                                <p style={{color: '#F48882'}}>0%</p>
                            </Col>
                        </Row>
                        <Row style={Rowboxstyle}>
                            <Col style={ColboxstyleLeft}>
                                <p style={ColBoxFontStyle}>Promo Price</p>
                                <p style={{color: '#F48882'}}>$0</p>
                            </Col>
                            <Col style={ColboxstyleRight}>
                                <p style={ColBoxFontStyle}>% Profit Margin (Promo)</p>
                                <p style={{color: '#F48882'}}>0%</p>
                            </Col>
                        </Row>

                        <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}} onClick={this.toggle}>
                            EDIT FILTER
                        </Button>
                    
                </Card.Content>
                <Modal isOpen={this.state.modal} toggle={this.toggle} size='lg' backdrop={false} className={this.props.className}>
                    <ModalHeader toggle={this.toggle} style={{fontSize: 17, paddingLeft: 25}}>PROMOTION RECOMMENDATION</ModalHeader>
                    <ModalBody style={{margin: 10}}>
                        <Row style={{marginTop: 15, marginBottom: 15}}>
                            <Col sm="7">
                                <Form>
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Promotion Type</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedPromotionType: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.promotionTypeOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>

                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Product</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedProduct: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.productOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>

                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>CHANNEL</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedChannel: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.channelOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    <Row>
                                        <Col>
                                            <Label style={{fontWeight: 'bold'}}>Promotion Start Date</Label>
                                        </Col>
                                        <Col>
                                                <Label style={{fontWeight: 'bold'}}>Promotion End Date</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <FormGroup>
                                                <DateTimePicker onChange={this.onChange0} value={this.state.date0}/>
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup>
                                                <DateTimePicker onChange={this.onChange1} value={this.state.date1}/>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Form>
                                <Button style={{width: 180, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                    GENERATE PROMO
                                </Button>
                                {/* <Button style={{width: 180, marginLeft:20, backgroundColor: 'gray', color: 'white',  borderColor: 'white'}} onClick= {  async (event, data) => {
                                                    await this.setState({
                                                        selectedChannel: "",
                                        selectedPromotionType: "",
                                        selectedSubcategory: "",
                                        selectedProduct: "",
                                                    })
                                                }
                                 }>
                                    RESET TO DEFAULT
                                </Button> */}
                            </Col>
                            <Col sm="1"/>
                            <Col sm="4">
                                <img src={salesOverTimePic} alt="campaign" style={{width: '100%'}}/>
                                <p style={{marginTop: 10, marginBottom: 10, fontSize: 16, fontWeight: 'bold'}}>Simulate Promo Sales</p>
                                Select the type of promotion you want to set, followed by the relevant products to apply the promotion on.
                                <br/><br/>
                                Finally, set the date range to run this promotion and the channel(s) to be applied to.
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal> 
            </Card>
        )
    }
}

export default PromotionRecommendation