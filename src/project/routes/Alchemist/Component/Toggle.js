
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button,
    Popover,
    PopoverBody,
} from 'reactstrap';
import { Card, Icon } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import '../../../assets/css/theme.css';
import alchemist4Pic from '../../../assets/image/alchemist4.PNG';

class Toggle extends Component {
    constructor(props) {
        super(props);
        this.toggle0 = this.toggle0.bind(this);
        this.state = {
            popoverOpen0: false,
        }
    }

    toggle0() {
        this.setState({
          popoverOpen0: !this.state.popoverOpen0
        });
    }

    render(){
        
        return(
            <div>
                <Icon size="large" name="info circle" color='grey' id="Popover1" onClick={this.toggle0} style={{marginTop: 10}}/>
                <Popover placement="bottom" isOpen={this.state.popoverOpen0} target="Popover1" toggle={this.toggle0}>
                    <PopoverBody>Description</PopoverBody>
                </Popover>
            </div>
        )
    }
}

export default Toggle