
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button
} from 'reactstrap';
import { Card, Icon } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import '../../../assets/css/theme.css';
import campaignPic from '../../../assets/image/campaign.jpg';
import alchemist1Pic from '../../../assets/image/alchemist1.PNG';
import alchemist2Pic from '../../../assets/image/alchemist2.PNG';
import alchemist3Pic from '../../../assets/image/alchemist3.PNG';
 
class Summary extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
        }
    }

    render(){
        return(
            <Card className="card-container">
                <Card.Content>
                    <div style={{marginTop: 40, marginBottom:40}}>
                        
                        <p style={{marginTop: 10, fontSize: 20}}>SALES FORECASTING</p>
                        <div>
                            <img src={alchemist1Pic} alt="campaign" style={{width: 130}}/>
                        </div>
                        <div>
                            <span className="popup2" data-popuptext="'Holt-Winters' is the default method used. Click to change method and adjust parameter(s)">
                                <Button style={{width: 200, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}} href="/salesforecasting">
                                    HOLT-WINTERS 
                                </Button>
                            </span>
                        </div>

                    </div>

                    <Row style={{marginTop: 40, marginBottom:40}}>
                        <Col>
                            <p style={{marginTop: 10, fontSize: 20}}>PRICE OPTIMIZATION</p>
                            <div>
                                <img src={alchemist2Pic} alt="campaign" style={{width: 130}}/>
                            </div>
                            <div>
                                <span className="popup2" data-popuptext="'Pricing Formula A' is the default method used. Click to change method and adjust parameter(s).">
                                    <Button style={{width: 200, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                        PRICING FORMULA A
                                    </Button>
                                </span> 
                            </div>
                        </Col>

                        <Col>
                            <p style={{marginTop: 10, fontSize: 20}}>PROMOTION RECOMMENDATION</p>
                            <div>
                                <img src={alchemist3Pic} alt="campaign" style={{width: 130}}/>
                            </div>
                            <div>
                                <span className="popup2" data-popuptext="'Formula B' is the default method used. Click to change method and adjust parameter(s).
">
                                    <Button style={{width: 200, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                        PRICING FORMULA B
                                    </Button>
                                </span> 
                            </div>
                        </Col>

                    </Row>
                </Card.Content>
            </Card>
        )
    }
}

export default Summary