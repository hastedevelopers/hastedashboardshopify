
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button,
    Nav,
    NavItem,
    NavLink,
    TabContent, 
    TabPane
} from 'reactstrap';
import { Card } from 'semantic-ui-react';
import classnames from 'classnames';
import '../../../assets/css/theme.css';
import TopHightlightTable from './TopHightlightTable';

class TopHightlight extends Component {
    constructor(props) {
        super(props);

        this.toggleTab = this.toggleTab.bind(this);
        
        this.state = {
            activeTab: '1',
            Promotion: [
                {name: 'Promotion A', quantity: 1, total: 180},
                {name: 'Promotion B', quantity: 2, total: 280},
                {name: 'Promotion C', quantity: 3, total: 380},
                {name: 'Promotion D', quantity: 4, total: 480},
                {name: 'Promotion E', quantity: 5, total: 580},
                {name: 'Promotion F', quantity: 6, total: 680},
                {name: 'Promotion G', quantity: 7, total: 780},
                {name: 'Promotion H', quantity: 8, total: 880},
                {name: 'Promotion I', quantity: 9, total: 980},
            ],
            Category: [
                {name: 'Category A', quantity: 1, total: 180},
                {name: 'Category B', quantity: 2, total: 280},
                {name: 'Category C', quantity: 3, total: 380},
                {name: 'Category D', quantity: 4, total: 480},
                {name: 'Category E', quantity: 5, total: 580},
                {name: 'Category F', quantity: 6, total: 680},
                {name: 'Category G', quantity: 7, total: 780},
                {name: 'Category H', quantity: 8, total: 880},
                {name: 'Category I', quantity: 9, total: 980},
            ],
            Subcategory: [
                {name: 'Subcategory A', quantity: 1, total: 180},
                {name: 'Subcategory B', quantity: 2, total: 280},
                {name: 'Subcategory C', quantity: 3, total: 380},
                {name: 'Subcategory D', quantity: 4, total: 480},
                {name: 'Subcategory E', quantity: 5, total: 580},
                {name: 'Subcategory F', quantity: 6, total: 680},
                {name: 'Subcategory G', quantity: 7, total: 780},
                {name: 'Subcategory H', quantity: 8, total: 880},
                {name: 'Subcategory I', quantity: 9, total: 980},
            ],
            Brand: [
                {name: 'Brand A', quantity: 1, total: 180},
                {name: 'Brand B', quantity: 2, total: 280},
                {name: 'Brand C', quantity: 3, total: 380},
                {name: 'Brand D', quantity: 4, total: 480},
                {name: 'Brand E', quantity: 5, total: 580},
                {name: 'Brand F', quantity: 6, total: 680},
                {name: 'Brand G', quantity: 7, total: 780},
                {name: 'Brand H', quantity: 8, total: 880},
                {name: 'Brand I', quantity: 9, total: 980},
            ],
            Product: [
                {name: 'Product A', quantity: 1, total: 180},
                {name: 'Product B', quantity: 2, total: 280},
                {name: 'Product C', quantity: 3, total: 380},
                {name: 'Product D', quantity: 4, total: 480},
                {name: 'Product E', quantity: 5, total: 580},
                {name: 'Product F', quantity: 6, total: 680},
                {name: 'Product G', quantity: 7, total: 780},
                {name: 'Product H', quantity: 8, total: 880},
                {name: 'Product I', quantity: 9, total: 980},
            ]
        }
    }

    toggleTab(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab,
          });
        }
    }

    render(){

        return(
            <Card className="card-container">
                <Card.Content>
                    <Card.Header className="card-header">TOP HIGHLIGHTS</Card.Header>
                    <Nav tabs style={{marginTop: 30}}>
                        <NavItem>
                            <NavLink
                            className={classnames( this.state.activeTab === '1'? 'active tab-header-active':'tab-header-notactive' )}
                            onClick={() => { this.toggleTab('1'); }}
                            >
                            Promotions
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                            className={classnames( this.state.activeTab === '2'? 'active tab-header-active':'tab-header-notactive' )}
                            onClick={() => { this.toggleTab('2'); }}
                            >
                            Category
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                            className={classnames( this.state.activeTab === '3'? 'active tab-header-active':'tab-header-notactive' )}
                            onClick={() => { this.toggleTab('3'); }}
                            >
                            Subcategory
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                            className={classnames( this.state.activeTab === '4'? 'active tab-header-active':'tab-header-notactive' )}
                            onClick={() => { this.toggleTab('4'); }}
                            >
                            Brand
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                            className={classnames( this.state.activeTab === '5'? 'active tab-header-active':'tab-header-notactive' )}
                            onClick={() => { this.toggleTab('5'); }}
                            >
                            Product
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                            <Row>
                                <Col sm="12">
                                    <TopHightlightTable data={this.state.Promotion} name="Promotion"/>
                                    <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                        VIEW ALL PROMOTIONS
                                    </Button>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tabId="2">
                            <Row>
                                <Col sm="12">
                                    <TopHightlightTable data={this.state.Category} name="Category"/>
                                    <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                        VIEW ALL CATEGORY
                                    </Button>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tabId="3">
                            <Row>
                                <Col sm="12">
                                    <TopHightlightTable data={this.state.Subcategory} name="Subcategory"/>
                                    <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                        VIEW ALL SUBCATEOGRY
                                    </Button>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tabId="4">
                            <Row>
                                <Col sm="12">
                                    <TopHightlightTable data={this.state.Brand} name="Brand"/>
                                    <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                        VIEW ALL BRAND
                                    </Button>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tabId="5">
                            <Row>
                                <Col sm="12">
                                    <TopHightlightTable data={this.state.Product} name="Product"/>
                                    <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                        VIEW ALL PRODUCT
                                    </Button>
                                </Col>
                            </Row>
                        </TabPane>
                    </TabContent>
                    
                </Card.Content>
            </Card>
        )
    }
}

export default TopHightlight