
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Popover,
    PopoverBody,
} from 'reactstrap';
import { Icon } from 'semantic-ui-react';
import '../../../assets/css/theme.css';

class Summary extends Component {
    constructor(props) {
        super(props);

        this.toggle0 = this.toggle0.bind(this);
        this.toggle1 = this.toggle1.bind(this);
        this.toggle2 = this.toggle2.bind(this);
        this.toggle3 = this.toggle3.bind(this);
        
        this.state = {
            popoverOpen0: false,
            popoverOpen1: false,
            popoverOpen2: false,
            popoverOpen3: false,
            ticketSize:undefined,
            basketSize:undefined,
            customer:undefined,
            transaction:undefined

        }
        console.log(this.props.summaryData);

    }

    componentWillReceiveProps(nextProps){
            const data  = this.props.summaryData;
            if(data!=undefined) {
                this.setState({ticketSize: data.average_ticket_size});
                this.setState({customer: data.unique_customers});
                this.setState({transaction: data.transactions});
                this.setState({basketSize: data.average_basket_size});
            }}


    toggle0() {
        this.setState({
          popoverOpen0: !this.state.popoverOpen0
        });
    }

    toggle1() {
        this.setState({
          popoverOpen1: !this.state.popoverOpen1
        });
    }

    toggle2() {
        this.setState({
          popoverOpen2: !this.state.popoverOpen2
        });
    }

    toggle3() {
        this.setState({
          popoverOpen3: !this.state.popoverOpen3
        });
    }

    render(){
        console.log("first");
        let iconsize= {
            position: 'absolute',
            top: 0
        }
        return(
            <Row className="div-row">
                <Col sm="3">
                    <h2>
                        AVG Ticket Size 
                        <Icon size="small" name="info circle" color='grey' id="Popover1" onClick={this.toggle0} className="summary-info-icon" style={iconsize}/>
                    </h2> 
                    <Popover placement="bottom" isOpen={this.state.popoverOpen0} target="Popover1" toggle={this.toggle0}>
                        <PopoverBody>Purchase amount per transaction</PopoverBody>
                    </Popover>
                    {/* <h3>${Number(this.state.ticketSize).toFixed(2)}</h3> */}
                    <h3>{this.state.ticketSize > 0 ?
                         "$" + Number(this.state.ticketSize).toLocaleString(navigator.language, { minimumFractionDigits: 2 })
                         :
                         "$0.00"
                         }
                    </h3>
                </Col>
                <Col sm="3">
                    <h2>
                        AVG Basket Size
                        <Icon size="small" name="info circle" color='grey' id="Popover2" onClick={this.toggle1} className="summary-info-icon" style={iconsize}/>
                    </h2> 
                    <Popover placement="bottom" isOpen={this.state.popoverOpen1} target="Popover2" toggle={this.toggle1}>
                        <PopoverBody>Units sold per transaction (receipt)</PopoverBody>
                    </Popover>
                    <h3>{this.state.basketSize > 0 ?
                         Number(this.state.basketSize).toLocaleString(navigator.language, { minimumFractionDigits: 0 })
                         :
                         0
                         }
                    </h3> 
                </Col>
                <Col sm="3">
                    <h2>
                        Total Transactions
                        <Icon size="small" name="info circle" color='grey' id="Popover3" onClick={this.toggle2} className="summary-info-icon" style={iconsize}/>
                    </h2> 
                    <Popover placement="bottom" isOpen={this.state.popoverOpen2} target="Popover3" toggle={this.toggle2}>
                        <PopoverBody>Total number of transactions (receipts) generated</PopoverBody>
                    </Popover>
                    <h3>{this.state.transaction > 0 ?
                         Number(this.state.transaction).toLocaleString(navigator.language, { minimumFractionDigits: 0 })
                         :
                         0
                         }
                    </h3> 
                </Col>
                <Col sm="3">
                    <h2>
                        Unique Transactions
                        <Icon size="small" name="info circle" color='grey' id="Popover4" onClick={this.toggle3} className="summary-info-icon" style={iconsize}/>
                    </h2> 
                    <Popover placement="bottom" isOpen={this.state.popoverOpen3} target="Popover4" toggle={this.toggle3}>
                        <PopoverBody>Total number of unique transactions (receipts) generated</PopoverBody>
                    </Popover>
                    <h3>{this.state.customer > 0 ?
                        Number(this.state.customer).toLocaleString(navigator.language, { minimumFractionDigits: 0 })
                        :
                        0
                        }
                    </h3>
                </Col>
            </Row>
        )
    }
}

export default Summary