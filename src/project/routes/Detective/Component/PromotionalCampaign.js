
import React, {Component} from 'react';
import { 
    Row, 
    Col,
    Button,
    Modal,  
    ModalHeader, 
    ModalBody, 
    ModalFooter,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText
} from 'reactstrap';
import { Card, Icon, Dropdown } from 'semantic-ui-react';
import { Line } from 'react-chartjs-2';
import DateTimePicker from 'react-datetime-picker'
import '../../../assets/css/theme.css';
import campaignPic from '../../../assets/image/campaign.jpg';

class PromotionalCampaign extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        
        this.state = {
            modal: false,
        }
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
    }

    render(){
        return(
            <Card className="card-container">
                <Card.Content>
                    <Card.Header className="card-header">PROMOTIONAL CAMPAIGN</Card.Header>
                    <div style={{marginTop: 40, marginBottom:40}}>
                        <img src={campaignPic} alt="campaign"/>
                        <p style={{marginTop: 10, fontSize: 24}}> Find out how is your promotion doing</p>
                        <p style={{marginTop: 10, marginBottom:30, fontSize: 18}}> Click below to start filtering</p>
                        <Button style={{width: 300, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}} onClick={this.toggle}>
                            SEARCH PROMO ID
                        </Button>
                    </div>
                </Card.Content>
                <Modal isOpen={this.state.modal} toggle={this.toggle} size='lg' backdrop={false} className={this.props.className}>
                    <ModalHeader toggle={this.toggle} style={{fontSize: 17, paddingLeft: 25}}>PROMOTIONAL CAMPAIGN FILTER</ModalHeader>
                    <ModalBody style={{margin: 10}}>
                        <Row style={{marginTop: 15, marginBottom: 15}}>
                            <Col sm="7">
                                <Form>

                                    <Row>
                                        <Col>
                                            <Label style={{fontWeight: 'bold'}}>From</Label>
                                        </Col>
                                        <Col>
                                                <Label style={{fontWeight: 'bold'}}>To</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <FormGroup>
                                                <DateTimePicker onChange={this.onChange0} value={this.state.date0}/>
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup>
                                                <DateTimePicker onChange={this.onChange1} value={this.state.date1}/>
                                            </FormGroup>
                                        </Col>
                                    </Row>

                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>CHANNEL</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedChannel: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.channelOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    
                                    <FormGroup>
                                        <Label style={{fontWeight: 'bold'}}>Product</Label>
                                        <Dropdown defaultValue={'1'} selection multiple
                                            onChange={
                                                async (event, data) => {
                                                    await this.setState({
                                                        selectedProduct: data.value
                                                    })
                                                }
                                            }
                                            options={
                                                this.state.productOptions
                                            } 
                                            className="form-control"
                                        />
                                    </FormGroup>
                                </Form>
                                <Button style={{width: 180, backgroundColor: '#f88186', color: 'white',  borderColor: 'white'}}>
                                    SEARCH
                                </Button>
                                <Button style={{width: 180, marginLeft:20, backgroundColor: 'gray', color: 'white',  borderColor: 'white'}} onClick= {  async (event, data) => {
                                                    await this.setState({
                                                        selectedChannel: "",
                                        selectedCategory: "",
                                        selectedSubcategory: "",
                                        selectedProduct: "",
                                                    })
                                                }
                                 }>
                                    RESET TO DEFAULT
                                </Button>
                            </Col>
                            <Col sm="1"/>
                            <Col sm="4">
                                <img src={campaignPic} alt="campaign" style={{width: '100%'}}/>
                                <p style={{marginTop: 10, marginBottom: 10, fontSize: 16, fontWeight: 'bold'}}>Track your promo's performance</p>
                                    Filter by channel(s), date range and product(s).
                                    <br/><br/>
                                    Then select a promotion by its unique ID to display how the selected promotion is performing against 
                                    <br/>
                                    sales of the product assuming if promotion is not run.
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal> 
            </Card>
        )
    }
}

export default PromotionalCampaign