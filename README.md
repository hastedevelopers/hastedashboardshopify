## HASTE Shopify version
*Haste Shopify Doc v0.0.1*

Hi all, An here. Hopefully one day Haste will be big enough that so many read this README (heheh). Anyways, here are a few general guidelines in editing this beautiful looking project. The aim for this doc is so that anybody hopping on this project can start hacking away asap, without violating any established coding guidelines. Feel free to update this document in the future :) :) :) :)

#### TLDR Must Dos
When in doubt, run `npm install`

Before styling, run `watch-css`

Don't write messy codes or fufei will eat u omnomnom

#### Useful commands

Other than the established `npm start` and the `npm build`, I've added 2 scripts to compile scss stylesheets (in the folder scss) into css style sheets (in the folder css). To call these commands, type in
```
npm run watch-css
npm run build-css
```
`watch-css` will automatically compile any scss style changes to the corresponding css file, so you should always run it when you're hacking away! :)

