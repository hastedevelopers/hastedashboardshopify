import React, { Component } from 'react';
import { 
    Container, 
    Row, 
    Col
} from 'reactstrap';
import { Dropdown } from 'semantic-ui-react';

import '../../assets/css/theme.css';
import TopNavBar from '../Component/TopNavBar';
import ProductFeatures from './Component/ProductFeatures';
import ShopperFeatures from './Component/ShopperFeatures';
import Summary from './Component/Summary';
import Wiki from './Component/Wiki';

export default class Alchemist extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            productFeatures: {
                0: { name: "Brand", active: false },
                1: { name: "Packaging", active: false },
                2: { name: "UOM", active: false },
                3: { name: "Expiry Or Out of Season", active: false },
                4: { name: "When was this added", active: false },
                5: { name: "Barcode or Product mapping", active: false },
                6: { name: "Category + Subcategory", active: false },
                7: { name: "Occasional", active: false },
                8: { name: "Placement  (Self or Site)", active: false },
                9: { name: "Channel", active: false },
                10: { name: "Channel2", active: false },
                11: { name: "Channel3", active: false }
            },
            shopperFeatures: {
                0: { name: "Gender", active: false },
                1: { name: "Age", active: false },
                2: { name: "Race/Religion", active: false },
                3: { name: "Home Address", active: false },
                4: { name: "IP Address", active: false },
                5: { name: "Time / Day", active: false },
                6: { name: "Category + Subcategory", active: false },
                7: { name: "Royalty (Membership tier)", active: false },
                8: { name: "Price Sensitivity", active: false },
                9: { name: "Marital Status 1", active: false },
                10: { name: "Marital Status 2", active: false },
                11: { name: "Marital Status 3", active: false }
            },
        };
      }



    render() {

        return(
            <div className="grad">
                <TopNavBar name="alchemist"/>
            
                <Container>
                    <Row className="div-row">
                        <Col>
                            <h1>Alchemist</h1>
                        </Col>
                    </Row>
                    <Row className="div-row">
                        <Summary/>
                    </Row>
                    <Row className="div-row">
                        <Col sm="6">
                            <ProductFeatures productFeatures = {this.state.productFeatures}/>
                        </Col>
                        <Col sm="6">
                            <ShopperFeatures shopperFeatures = {this.state.shopperFeatures}/>
                        </Col>
                    </Row>
                    <Row className="div-row">
                        <Wiki/>
                    </Row>
                </Container>
            
            </div>
        )
    }
}